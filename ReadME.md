# Build this framework using below technology stack
.net core framework
c#
NUnit unit testing framework
specflow
selenium
restsharp
aws sdk


# To build the code after any changes, run this command on your terminal once at the project location
dotnet build
# To run the test for all features files, run this command on your terminal once at the project location
dotnet test 

# Use this command to run the tests and generate html report for test results in the root Reports folder
dotnet test --logger "html;logfilename=TestResults_1.html" --results-directory Reports

# I have added a bash script file on this project folder "bashScript"
# Just click on this file on your system and it should run all the steps from feature files and generate a new test results report in the Reports folder (I have tested this on my mac machine only)
﻿using System;
using NUnit.Framework;
using RestSharp;
//using ERM_Automation.Utilities;

using RestSharp.Authenticators;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;

namespace ERM_Automation.Tests
{
    public class AWSApiGateWayTest
    {
        public RestClient restClient;
        public RestRequest restRequest;
        public IRestResponse restResponse;
        string response;

        public void Authenticate(string header, string key, string call)
        {
            /**  Sending GET request when I'm using the api key **/
            //Console.WriteLine("AWS Api gate way from data and api config file.." + Data.AWSApiGateWay);
            //restClient = new RestClient(Data.AWSApiGateWay)
            restClient = new RestClient("https://erstfr54dd.execute-api.ap-southeast-2.amazonaws.com/test/pets")
            {
                Authenticator = new HttpBasicAuthenticator(header, key)
            };

            if (call == "GET")
            {
                restRequest = new RestRequest(Method.GET);

            }
            else if (call == "POST")
            {
                restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("Accept", "application/json");
                var body = new { type = "Test", price = 23 };
                restRequest.AddJsonBody(body);
            }

        }
        /** To get the  status code from response  **/
        public void SendRequest(string statusCode)
        {

            int expectedCode = int.Parse(statusCode);

            restResponse = restClient.Execute(restRequest);

            //Extract output from response
            response = restResponse.Content;


            //Extract response code from response
            int responseCode = (int)restResponse.StatusCode;
            Assert.AreEqual(expectedCode, responseCode, "Response code is " + responseCode);

        }

        /**  Validate the three items exist**/

        public void ValidateItems(int items)
        {

            dynamic petsResponse = JsonConvert.DeserializeObject(response);
            Assert.AreEqual(items, petsResponse.Count, "incorrect count");

        }

        /**  Validate the success or error message **/
        public void ValidateMessage(string statusMessage)
        {

            dynamic petsResponse = JsonConvert.DeserializeObject(response);
            string name = petsResponse.message;

            // Validate the response
            Assert.AreEqual(name, statusMessage, "Correct message received in the Response");

        }


        /**  Sending GET request when user not using the api key **/
        public void AuthenticationWithOutKEy()
        {
            restClient = new RestClient("https://erstfr54dd.execute-api.ap-southeast-2.amazonaws.com/test/pets");
            restRequest = new RestRequest(Method.GET);
        }

        /**  Executing the request and getting the reponse **/

        public void SendRequestWithOutKey()
        {

            restResponse = restClient.Execute(restRequest);

            //Extract output from response
            response = restResponse.Content;


        }

    }
}

﻿
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using NUnit.Framework;
using System;
using System.IO;
using Newtonsoft.Json;
using Amazon;

namespace ERM_Automation.Tests
{
    class S3BucketTest
    {
        private const string accessKey = "AKIAUBWUCV7QYA7XZOHJ";
        private const string secretKey = "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";
        private static readonly RegionEndpoint bucketRegion = Amazon.RegionEndpoint.APSoutheast2;
        private string keyName;
        ListObjectsRequest request;
        ListObjectsResponse response;
        AmazonS3Client s3Client;
        string responseBody;
        GetObjectResponse getObjectResponse;
        GetObjectRequest getObjectRequest;
        StreamReader reader;
        Stream responseStream;


        /** sending the bucket name**/
        
        /** Log onto AWS and verifying that the erm-testautoengineer-test bucket exist and has atleast one item in there**/
        public void LogInAndVerifyS3BucketItems(string bucketName)
        {
            s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey), bucketRegion);
            request = new ListObjectsRequest();
            request.BucketName = bucketName;
            var res = s3Client.ListObjectsAsync(request);
            foreach (S3Object o in res.Result.S3Objects)
            {
                if (o.BucketName == bucketName)
                {
                    Console.WriteLine("{0}", o.BucketName, o.Key, o.Size, o.LastModified);
                    break;
                }

            }

        }

        /** Read the item from s3 bucket **/
        public void getBucketContent(string image, string bucketName)
        {
            getObjectRequest = new GetObjectRequest();
            getObjectRequest.BucketName = bucketName;
            getObjectRequest.Key = image;
            var getObjectRes = s3Client.GetObjectAsync(getObjectRequest);
            responseStream = getObjectRes.Result.ResponseStream;
            reader = new StreamReader(responseStream);
            responseBody = reader.ReadToEnd();
        }

        /** Verify that userid=1 exist in the item **/
        public void verifyTheItem(string item)
        {

            int expectedItemValue = int.Parse(item);
            dynamic s3bucketResponse = JsonConvert.DeserializeObject(responseBody);
            int userId = s3bucketResponse[0].userId;
            Console.WriteLine(userId);
            Assert.AreEqual(expectedItemValue, userId, "not correct");

        }
    }
}

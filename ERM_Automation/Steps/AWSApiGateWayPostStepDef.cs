﻿
using ERM_Automation.Tests;
using TechTalk.SpecFlow;

namespace ERM_Automation.Steps
{
    [Binding]
    public class AWSApiGateWayPostStepDef
    {
        AWSApiGateWayTest gateWayTest = new AWSApiGateWayTest();

        [Given(@"I make a POST call to API Gateway with correct key ""(.*)"" and ""(.*)""")]
        public void GivenIIMakeCallToAPIGateway(string header, string key)
        {

            gateWayTest.Authenticate(header, key, "POST");
        }

        [When(@"I get ""(.*)"" status for POST")]
        public void WhenIGetResponseStatus(string statusCode)
        {
            gateWayTest.SendRequest(statusCode);
        }

        [Then(@"I confirm that response body has ""(.*)""")]

        public void ThenTheResultShouldBe(string statusMessage)
        {
            gateWayTest.ValidateMessage(statusMessage);

         
        }
    }
}

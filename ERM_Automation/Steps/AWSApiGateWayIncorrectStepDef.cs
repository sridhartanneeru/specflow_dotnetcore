﻿
using ERM_Automation.Tests;

using TechTalk.SpecFlow;

namespace ERM_Automation.Steps
{
    [Binding]
    public class AWSApiGateWayIncorrectStepDef
    {
        AWSApiGateWayTest gateWayTest = new AWSApiGateWayTest();

        [Given(@"I make GET call to API Gateway without key")]
        public void GivenIIMakeCallToAPIGateway()
        {
            gateWayTest.AuthenticationWithOutKEy();

        }

        [Then(@"I confirm that response body contains error message ""(.*)""")]

        public void ThenTheResultShouldBe(string error)
        {
            gateWayTest.SendRequestWithOutKey();
            gateWayTest.ValidateMessage(error);


        }
    }
}

﻿using ERM_Automation.Tests;
using TechTalk.SpecFlow;

namespace ERM_Automation.Steps
{
    [Binding]
    public class S3BucketStepDef
    {
        S3BucketTest s3BucketTest = new S3BucketTest();


        [Given(@"I log onto AWS successfully and verify that bucket ""(.*)"" exists")]
        public void GivenILogOntoAWSAndTheBucketExists(string bucketName)
        {
             s3BucketTest.LogInAndVerifyS3BucketItems(bucketName);
        }

        [Then(@"I see that the ""(.*)"" exist in the bucket ""(.*)""")]
        public void ThenISeeThatTheItemExistInThatBucket(string image, string bucketName)
        {
            s3BucketTest.getBucketContent(image, bucketName);


        }
        [Then(@"I verify that item has userId ""(.*)""")]
        public void ThenIVerifyThatItemHasUserID(string item)
        {
            s3BucketTest.verifyTheItem(item);


        }

    }
}

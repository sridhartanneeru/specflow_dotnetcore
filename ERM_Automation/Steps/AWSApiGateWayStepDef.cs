﻿using ERM_Automation.Tests;
using TechTalk.SpecFlow;


namespace ERM_Automation.Steps
{
    [Binding]
    public class AWSApiGateWay
    {
        AWSApiGateWayTest gateWayTest = new AWSApiGateWayTest();

        [Given(@"I make a GET call to API Gateway with correct key ""(.*)"" and ""(.*)""")]
        public void GivenIIMakeCallToAPIGateway(string header, string key)
        {

            gateWayTest.Authenticate(header, key, "GET");
        }

        [When(@"I get ""(.*)"" status for GET")]
        public void WhenIGetResponseStatus(string statusCode)
        {
            gateWayTest.SendRequest(statusCode);
        }

        [Then(@"I confirm that response contains ""(.*)"" items")]
        public void ThenTheResultShouldBe(int items)
        {
            gateWayTest.ValidateItems(items);

        }
    }
}

using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

using ERM_Automation.Pages;

namespace ERM_Automation.Steps
{
    [Binding]
    public class LoginSteps
    {
        private readonly IWebDriver _webDriver;

        public LoginSteps(ScenarioContext scenarioContext)
        {
            _webDriver = scenarioContext["WEB_DRIVER"] as IWebDriver;
        }
         LoginPage loginPage;
         HomePage homePage;

        [Given(@"the user is on the home page")]
        public void GivenLaunchBrowserAndNavigateUrl()
        {
            homePage = new HomePage(_webDriver);
            homePage.goToPage();
        }

        [Then(@"they see the page title contains ""(.*)""")]
        public void ThenIseeThePageTitleContains(string expectedTitle)
        {
            Assert.AreEqual(homePage.pageTitle(), expectedTitle);    
            homePage.clickLoginLink();
            loginPage = new LoginPage(_webDriver);
            Assert.AreEqual(loginPage.pageTitle(), "nopCommerce demo store. Login");
        }

        [When(@"Enter ""(.*)"" and ""(.*)"" details")]
        public void WhenEnterAndDetails(String emailAddress, String Password)
        {
            loginPage.enterUserName(emailAddress);
            loginPage.enterPassword(Password);
        }

        [When(@"click on Sign In button")]
        public void ThenClickOnSignInButton()
        {
            loginPage.clickLoginBtn();
        }

        [Then(@"the login unsuccessful message is displayed")]
        public void ThenTheUnsuccessfulLogin()
        {
            Assert.AreEqual("Login was unsuccessful. Please correct the errors and try again.\nNo customer account found", loginPage.inValidErrorMessage());
           
        }

    }
}
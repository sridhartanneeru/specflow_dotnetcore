using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using ERM_Automation.Pages;

namespace ERM_Automation.Steps
{
    [Binding]
    public class SearchSteps
    {
        private readonly IWebDriver _webDriver;

        public SearchSteps(ScenarioContext scenarioContext) {
            _webDriver = scenarioContext["WEB_DRIVER"] as IWebDriver;
        }
         LoginPage loginPage;
         HomePage homePage;
         SearchPage searchPage;


        [Given(@"the user searches for ""(.*)""")]
        public void GivenTheUserSearchesFor(string str)
        {
            homePage = new HomePage(_webDriver);
            Assert.AreEqual(homePage.isSearchBoxEnabled(), true);
            homePage.enterSearchByText(str);
            homePage.clickSearchButton();
            
        }

        [Then(@"verify that the search result is returned with ""(.*)""")]
        public void verifyThatTheSearchResultIsReturnedWith(string str)
        {
            searchPage = new SearchPage(_webDriver);
            Assert.AreEqual("Asus N551JK-XO076H Laptop\n$1,500.00", searchPage.noSearchResultTextMessage());
            
        }

        [When(@"add the item to the cart")]
        public void addTheItemToTheCart()
        {
            Assert.AreEqual(true, searchPage.isAddToCartBtnDisplayed());
            Task.Delay(3000).Wait();
            searchPage.clickOnAddToCart();
            Task.Delay(3000).Wait();
        }

        [Then(@"verify that the confirmation message is displayed with ""(.*)""")]
        public void verifyThatTheConfirmationMessageIsDisplayedWith(string str)
        {
            
           Assert.AreEqual(str, searchPage.getConfirmationMessage());
        }

    }
}
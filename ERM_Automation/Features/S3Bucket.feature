﻿Feature: AWS S3 bucket feature
	
        Scenario: Verify AWS S3 for the bucket exists with at least 1 item and the user id of that item is "1"
            Given I log onto AWS successfully and verify that bucket "erm-testautoengineer-test" exists
             Then I see that the "samplefile_9d0ccca0-ac99-44b2-aae6-edfa0d18c1b5.json" exist in the bucket "erm-testautoengineer-test"
              And I verify that item has userId "1"

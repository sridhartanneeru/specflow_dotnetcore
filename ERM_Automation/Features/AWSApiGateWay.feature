﻿Feature: API Gateway feature
	
Scenario: 01. Make a successful GET call
	  Given I make a GET call to API Gateway with correct key "x-api-key" and "BpBMJjKxxc74hsr0dB5mqX5OCKCDaDCVN3HTifZgk"
	  When I get "200" status for GET
	  Then I confirm that response contains "3" items

Scenario: 02. Make an UnSuccessful GET call
	  Given I make GET call to API Gateway without key
	  Then I confirm that response body contains error message "Forbidden"


Scenario: 03. Make a successful POST call
	Given I make a POST call to API Gateway with correct key "x-api-key" and "BpBMJjKxxc74hsr0dB5mqX5OCKCDaDCVN3HTifZgk"
	 When I get "200" status for POST
	 Then I confirm that response body has "success"

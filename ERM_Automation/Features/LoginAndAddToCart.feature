@Chrome
Feature: Login and Add to Cart
        Background: Navigate to home page
            Given the user is on the home page
 
        Scenario: 01. Verify the login was unsuccessful with invalid username/pwd
             Then they see the page title contains "nopCommerce demo store"
             When Enter "sunithakallepu@gmail.com" and "abcd1234" details
              And click on Sign In button
             Then the login unsuccessful message is displayed

        Scenario: 02. Search for product and verify the price
              And the user searches for "Asus N551JK-XO076H Laptop"
             Then verify that the search result is returned with "Asus N551JK-XO076H Laptop\n$1,500.00"

        Scenario: 03. Add the product to the cart and verify the message appears
              And the user searches for "Asus N551JK-XO076H Laptop"
             Then verify that the search result is returned with "Asus N551JK-XO076H Laptop\n$1,500.00"
             When add the item to the cart
             Then verify that the confirmation message is displayed with "The product has been added to your shopping cart"
           
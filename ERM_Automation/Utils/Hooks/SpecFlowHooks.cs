using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ERM_Automation.Utils.Hooks
{
    [Binding]
    internal static class SpecFlowHooks
    {
        public static IWebDriver CurrentDriver;
        [Before]
        [Scope(Tag = "Chrome")]
        internal static void StartChromeDriver(ScenarioContext context)
        {
            var options = new ChromeOptions();
            CurrentDriver = new ChromeDriver("../../../../_drivers", options);
            context["WEB_DRIVER"] = CurrentDriver;
        }

        [Before]
        [Scope(Tag = "Firefox")]
        internal static void StartFirefoxDriver(ScenarioContext context)
        {
            // place holder for firefox driver initiation
            
        }

        [After]
        [Scope(Tag = "Chrome")]
        internal static void StopWebDriver()
        {
            CurrentDriver.Quit();
        }
        
    }
}
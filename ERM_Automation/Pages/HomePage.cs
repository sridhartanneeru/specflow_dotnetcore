using System;
using OpenQA.Selenium;

namespace ERM_Automation.Pages
{
    public class HomePage
    {

        private IWebDriver _driver;
        public HomePage(IWebDriver driver)
        {
            _driver = driver;
        }
        public void clickLoginLink()
        {
            _driver.FindElement(By.ClassName("ico-login")).Click();
            
        }

        public String pageTitle() {
            return _driver.Title;
        }

        public void enterSearchByText(String str) {
            _driver.FindElement(By.XPath("//input[@placeholder='Search store']")).SendKeys(str);
        }

        public bool isSearchBoxEnabled() {
            return _driver.FindElement(By.XPath("//input[@placeholder='Search store']")).Enabled;
        }
        public void clickSearchButton() {
            _driver.FindElement(By.XPath("//input[@value='Search']")).Click();
        }

        public void goToPage() {
            _driver.Navigate().GoToUrl("https://demo.nopcommerce.com");
            _driver.Manage().Window.Maximize();
        }

    }
}
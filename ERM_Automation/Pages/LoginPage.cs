using System;
using OpenQA.Selenium;
using System.Threading.Tasks;

namespace ERM_Automation.Pages
{
    public class LoginPage
    {
        private IWebDriver _driver;

        public LoginPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public String pageTitle()
        {
            return _driver.Title;
        }

        public void enterUserName(string text)
        {
            Task.Delay(3000).Wait();
            _driver.FindElement(By.XPath("//input[@id='Email']")).SendKeys(text);
        }
        public void enterPassword(string text)
        {
            Task.Delay(3000).Wait();
            _driver.FindElement(By.XPath("//input[@id='Password']")).SendKeys(text);

        }
        public void clickLoginBtn()
        {
            _driver.FindElement(By.XPath("//input[@value='Log in']")).Click();
            Task.Delay(3000).Wait();
        }
        public String inValidErrorMessage()
        {
            Task.Delay(3000).Wait();
            return _driver.FindElement(By.XPath("//div[@class='message-error validation-summary-errors']")).Text;

        }

    }
}

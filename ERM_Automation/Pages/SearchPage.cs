using System;
using OpenQA.Selenium;

namespace ERM_Automation.Pages
{
    public class SearchPage
    {

        private IWebDriver _driver;
        public static String titleText = "nopCommerce demo store. Search";
        public SearchPage(IWebDriver driver)
        {
            _driver = driver;
        }
       
        public By noSearchResult = By.XPath("//div[@class='search-results']/div");

        
        public String pageTitle() {
            return _driver.Title;
        }

        public bool isAddToCartBtnDisplayed() {
            return _driver.FindElement(By.XPath("//input[@value='Add to cart']")).Displayed;
        }
        public void clickOnAddToCart() {
            _driver.FindElement(By.XPath("//input[@value='Add to cart']")).Click();
        }

        public String getConfirmationMessage() {
            return _driver.FindElement(By.XPath("//div[@id='bar-notification']//p")).Text;
        }

        public String noSearchResultTextMessage() {
            return _driver.FindElement(By.XPath("//div[@class='search-results']/div")).Text;
        }

    }
}